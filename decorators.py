import asyncio
import random
import time


@asyncio.coroutine
def coroutine_task(iteraction):
    process_time = random.randint(1, 5)
    yield from asyncio.sleep(process_time)
    print(f'Iteracao {iteraction}, tempo decorrido: {process_time}')


@asyncio.coroutine
def coroutine_task_00():
    tasks = []
    for iteraction in range(10):
        tasks.append(asyncio.create_task(
            coroutine_task(iteraction)))

        wait_tasks = asyncio.wait(tasks)
        yield from wait_tasks


loop = asyncio.get_event_loop()  # Event Loop
loop.run_until_complete(coroutine_task_00())
loop.close()
